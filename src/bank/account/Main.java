package bank.account;

import java.util.Scanner;

/**
 * Allows to perform banking operations on accounts with cards.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03.24.2020
 */
public class Main {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int selection;
        final Account accountSberbankVP = new Account("1234123412341234", "Vasya Pupkin", 1000);
        final Account.Card cardMIR = accountSberbankVP.new Card("1234 1234 1234 1234");
        final Account.Card cardMastercard = accountSberbankVP.new Card("1234 1234 1234 1235");
        final Account accountVTBVP = new Account("1234123412341235", "Vasya Pupkin");
        final Account.Card cardVTB = accountVTBVP.new Card("1234 1234 1234 1236");

        do {
            selection = input("\n1 - Sberbank\n2 - VTB\nPlease, select an account number: ");

            Account currentAccount = (selection == 1) ? accountSberbankVP : accountVTBVP;
            Account.Card currentCard = cardVTB;

            if (currentAccount.equals(accountSberbankVP)) {
                selection = input("\n1 - MIR\n2 - Mastercard\nPlease, select a card number: ");

                currentCard = (selection == 1) ? cardMIR : cardMastercard;
            }

            System.out.println("Balance: " + currentAccount.getAmount());

            selection = input("\n1 - Deposit\n2 - Withdraw\nPlease, select an operation: ");

            if (selection == 1) {
                System.out.print("\nPlease, enter a money's amount to deposit: ");
                System.out.println("Deposited: " + currentCard.deposit(input.nextInt()));
            } else {
                System.out.print("\nPlease, enter a money's amount to withdraw: ");
                System.out.println("Withdrawn: " + currentCard.withdraw(input.nextInt()));
            }

            System.out.println("Balance: " + currentAccount.getAmount());

            selection = input("\n1 - Yes\n2 - No\nDo you wish to continue?\n");
        }
        while(selection == 1);
    }

    /**
     * Checks {@code selection} validity.
     *
     * @param message a message to output.
     * @return {@code selection}.
     */
    private static int input(String message) {
        int selection;

        do {
            System.out.print(message);
            selection = input.nextInt();
        }
        while(selection > 2 || selection < 1);

        return selection;
    }
}