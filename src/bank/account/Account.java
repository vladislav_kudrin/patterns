package bank.account;

public class Account {
    private final String number;
    private final String owner;
    private int amount;

    Account(final String number, final String owner) {
        this.number = number;
        this.owner = owner;
    }

    Account(final String number, final String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    private int withdraw(int amountToWithdraw) {
        if(amountToWithdraw < 0) {
            return 0;
        }
        else if(amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }

        amount = amount - amountToWithdraw;
        return amountToWithdraw;
    }

    public int getAmount() {
        return amount;
    }

    public String toString() {
        return "Owner: " + owner +
                "[Account number: " + number +
                ", Balance: " + amount +
                ']';
    }

    private int deposit(int amountToDeposit) {
        if(amountToDeposit < 0) {
            return 0;
        }

        amount = amount + amountToDeposit;
        return amountToDeposit;
    }

    class Card {
        private final String number;

        Card(final String number) {
            this.number = number;
        }

        int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        int deposit(int amountToDeposit) {
            return Account.this.deposit(amountToDeposit);
        }

        String getNumber() {
            return number;
        }
    }
}