package ice.cream.cafe.menu;

/**
 * Shows a result of smoothie selection.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public class Smoothie implements Drink{
    public void make() {
        System.out.println("\nSmoothie is made");
    }
}