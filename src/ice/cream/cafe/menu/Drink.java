package ice.cream.cafe.menu;

/**
 * Stores interface's methods.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public interface Drink {
    void make();
}