package ice.cream.cafe.menu;

/**
 * Shows a result of yogurt selection.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public class Yogurt implements Drink{
    public void make() {
        System.out.println("\nYogurt is made");
    }
}