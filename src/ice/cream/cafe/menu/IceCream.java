package ice.cream.cafe.menu;

/**
 * Shows a result of ice cream selection.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public class IceCream implements Drink{
    public void make() {
        System.out.println("\nIce cream is made");
    }
}