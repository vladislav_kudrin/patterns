package ice.cream.cafe;

import ice.cream.cafe.menu.*;
import ice.cream.cafe.toppings.*;
import java.util.Scanner;

/**
 * Shows result of drink's and topping's selections.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public class Main {
    public static void main(String[] args) {
        Drink drink;
        Scanner input = new Scanner(System.in);
        byte selection;

        do {
            System.out.print("\n0 - Ice cream; 1 - Yogurt; 2 - Smoothie\nPlease, select a drink: ");

            selection = input.nextByte();
        }
        while(selection < 0 || selection > 2);

        switch(selection) {
            case 1:
                drink = new Yogurt();
                break;
            case 2:
                drink = new Smoothie();
                break;
            default:
                drink = new IceCream();
                break;
        }

        do {
            System.out.print("\n0 - Nothing; 1 - Syrup; 2 - Sprinkles; 3 - Syrup and sprinkles\nPlease, select a topping: ");

            selection = input.nextByte();
        }
        while(selection < 0 || selection > 3);

        switch(selection) {
            case 1:
                drink = new Syrup(drink);
                break;
            case 2:
                drink = new Sprinkles(drink);
                break;
            case 3:
                drink = new Syrup(new Sprinkles(drink));
                break;
            default:
                break;
        }

        drink.make();
    }
}