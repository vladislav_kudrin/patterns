package ice.cream.cafe.toppings;

import ice.cream.cafe.menu.Drink;

/**
 * Shows a result of sprinkles' selection.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public class Sprinkles extends Topping{
    public Sprinkles(Drink drink) {
        super(drink);
    }

    public void topping() {
        System.out.println("-includes sprinkles");
    }
}