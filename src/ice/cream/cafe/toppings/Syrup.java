package ice.cream.cafe.toppings;

import ice.cream.cafe.menu.Drink;

/**
 * Shows a result of syrup selection.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public class Syrup extends Topping{
    public Syrup(Drink drink) {
        super(drink);
    }

    public void topping() {
        System.out.println("-includes syrup");
    }
}