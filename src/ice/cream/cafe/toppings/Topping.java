package ice.cream.cafe.toppings;

import ice.cream.cafe.menu.Drink;

/**
 * Stores abstract method, implements Drink.java interface.
 *
 * @author Vladislav
 * @since 06/07/2020
 * @version 1.0
 */
public abstract class Topping implements Drink {
    private Drink drink;

    Topping(Drink drink) {
        this.drink = drink;
    }

    public abstract void topping();

    public void make() {
        drink.make();
        topping();
    }
}