package file.obfuscator;

/**
 * Reads a code file located at {@code path}, delete all comments, spaces and saves changes to a new file that will located at {@code newPath}.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03.14.2020
 */
public class Main {
    public static void main(String[] args) {
        String path = "src/file/obfuscator/FileObfuscator.java";
        String newPath = "src/file/obfuscator/NewFileObfuscator.java";
        FileObfuscator fileObfuscator = new FileObfuscator(path, newPath);

        fileObfuscator.obfuscate();
    }
}