package file.obfuscator;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Reads a code file located at {@code path}, delete all comments, spaces and saves changes to a new file that will located at {@code newPath}.
 *
 * @author Vladislav
 * @version 1.0
 * @since  03.14.2020
 */
class FileObfuscator {
    private String path;
    private String newPath;

    /**
     *
     *
     * @param path a start code file's path.
     * @param newPath a new code file's path.
     */
    FileObfuscator(String path, String newPath) {
        this.path = path;
        this.newPath = newPath;
    }

    void obfuscate() {
        String code = "";
        String fileName, newFileName;

        code = readFile(code, path);

        code = deleteComments(code);

        code = deleteNewLines(code);

        code = deleteSpaces(code);

        fileName = getFileName(path);
        newFileName = getFileName(newPath);

        code = rename(fileName, newFileName, code);

        saveFile(newPath, code);
    }

    /**
     * Gets {@code fileName} from {@code path}.
     *
     * @param path a path to a code file.
     * @return a code file's name.
     */
    private String getFileName(String path) {
        String[] splitPath = path.split("/");
        String[] fileName = splitPath[splitPath.length - 1].split("\\.");

        return fileName[0];
    }

    /**
     * Reads code file from {@code path} and saves it to {@code code}.
     *
     * @param code a program's code.
     * @param path a path to a code file.
     */
    private String readFile(String code, String path) {
        String string;

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            while ((string = bufferedReader.readLine()) != null) {
                code = code.concat(string + "\n");
            }
        }
        catch(IOException e) {
            System.out.println("File not found!");
        }

        return code;
    }

    /**
     * Replaces all spaces to just one space in {@code code} and returns a result.
     *
     * @param code a program's code.
     * @return a program's code with replaced spaces.
     */
    private String deleteSpaces(String code) {
        return code.replaceAll("\\s+", " ");
    }

    /**
     * Deletes all comments in {@code code} and returns result.
     *
     * @param code a program's code.
     * @return a program's code with deleted comments.
     */
    private String deleteComments(String code) {
        return code.replaceAll("(/\\*(.|[\\n\\r])*?\\*/)|([^\"()\\[\\]]//.*[\\n\\r])", "");
    }

    /**
     * Deletes all newLine characters in {@code code} and returns a result.
     *
     * @param code a program's code.
     * @return a program's code with deleted newLine characters.
     */
    private String deleteNewLines(String code) {
        return code.replaceAll("\\n", "");
    }

    /**
     * Rename a class-name and a constructor-name in {@code code} and returns a result.
     *
     * @param fileName a code file's name.
     * @param code a program's code.
     * @return a program's code with renamed a class-name and a constructor-name.
     */
    private String rename(String fileName, String newFileName, String code) {
        code = code.replaceAll("[^/\"]" + fileName, " " + newFileName);
        return code;
    }

    /**
     * Creates a new code file with a new path.
     *
     * @param newPath a new code file's path.
     * @param code a program's code.
     */
    private void saveFile(String newPath, String code) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newPath))) {
            bufferedWriter.write(code);
        }
        catch(IOException e) {
            System.out.println("File not created!");
        }
    }
}